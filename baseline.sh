 #!/bin/bash

 #Baseline script for running anytime system changes are made

 DATE=`date +%d-%h-%y:%H%M`
 USER=`whoami`
 FILENAME=/tmp/$USER-baseline_$DATE

 echo "============> Creating $FILENAME for $USER =======> cat /tmp/$FILENAME for updated system baseline.     "
 
 netstat -tuln >$FILENAME
 lsof -i | grep TCP | grep LISTEN >> $FILENAME 
 lsof -i | grep UDP >> $FILENAME 
 ps -ef | awk '{ print $1" "$8" "$9}' >> $FILENAME
 cat /etc/passwd >> $FILENAME
 dpkg --get-selections | awk `{print $1 }` >> $FILENAME

 ## Uncomment the following line if you use ssh
 # cat /etc/ssh/sshd_config | grep -v "^#" | grep -v "^$" >> $FILENAME
 
 ## Uncomment the following two (2) lines if you use iptables
 #cat /etc/sysconfig/iptables >> $FILENAME 
 #iptables -L -n >> $FILENAME

